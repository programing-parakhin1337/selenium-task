package com.epam.tat.module4.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class YOPMailGeneratedPage extends AbstractPage {
    public YOPMailGeneratedPage(WebDriver driver) {
        super(driver);
    }

    public YOPMailCheckPage checkMail() {
        List<WebElement> elements = new WebDriverWait(driver, TIMEOUT).until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//*[@class='md but text f24 egenbut']")));
        elements.get(2).click();
        return new YOPMailCheckPage(driver);
    }
}
