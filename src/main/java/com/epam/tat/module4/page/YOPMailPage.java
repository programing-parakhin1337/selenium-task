package com.epam.tat.module4.page;

import com.epam.tat.module4.utils.configProperties;
import com.epam.tat.module4.waits.CustomConditions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class YOPMailPage extends AbstractPage {
    private static final String PAGE_URL = configProperties.getProperty().getProperty("YOPMailURL");
    @FindBy(xpath = "//*[@href='email-generator']")
    private WebElement randomEmailButton;

    public YOPMailPage(WebDriver driver) {
        super(driver);
    }

    public YOPMailGeneratedPage getEmailPage() {
        new WebDriverWait(driver, TIMEOUT).until(ExpectedConditions.elementToBeClickable(randomEmailButton));
        randomEmailButton.click();
        return new YOPMailGeneratedPage(driver);
    }

    public YOPMailPage openPage() {
        driver.get(PAGE_URL);
        new WebDriverWait(driver, TIMEOUT).until(CustomConditions.JSpageReady());
        return this;
    }
}
